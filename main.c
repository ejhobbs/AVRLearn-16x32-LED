#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>
#include "main.h"

/* Start with matrix being off, each position has 3 values for R, G and B */
char matrix[M_HEIGHT][M_WIDTH] = {
/* 0  */  {G,G,G,0,G,G,G,G,0,G,G,G,G,0,G,G,G,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
/* 1  */  {G,0,0,0,G,0,0,0,0,G,0,0,G,0,G,0,0,G,0,0,0,B,0,0,0,B,0,0,0,B,0,0},
/* 2  */  {G,G,G,0,G,G,G,G,0,G,0,0,G,0,G,0,0,G,0,0,B,0,0,0,0,B,0,0,0,0,B,0},
/* 3  */  {0,0,G,0,G,G,G,G,0,G,0,0,G,0,G,0,0,G,0,0,B,0,0,0,0,B,0,0,0,0,B,0},
/* 4  */  {0,0,G,0,G,0,0,0,0,G,0,0,G,0,G,0,0,G,0,0,B,0,R,0,B,0,B,0,R,0,B,0},
/* 5  */  {G,G,G,0,G,G,G,G,0,G,0,0,G,0,G,G,G,0,0,0,0,B,B,B,0,0,0,B,B,B,0,0},
/* 6  */  {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
/* 7  */  {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
/* 8  */  {R,R,R,R,0,R,0,0,R,0,R,R,R,0,0,R,R,R,R,0,R,R,R,0,B,B,B,0,B,B,B,0},
/* 9  */  {R,0,0,R,0,R,0,0,R,0,R,0,0,R,0,R,0,0,0,0,R,0,0,0,B,0,0,B,0,0,B,0},
/* 10 */  {R,0,0,R,0,R,0,0,R,0,R,0,0,R,0,R,R,R,R,0,R,R,R,0,B,0,0,0,0,0,B,0},
/* 11 */  {R,0,0,R,0,R,0,0,R,0,R,0,0,R,0,R,R,R,R,0,0,0,R,0,0,B,B,0,B,B,0,0},
/* 12 */  {R,0,0,R,0,R,0,0,R,0,R,0,0,R,0,R,0,0,0,0,0,0,R,0,0,0,B,0,B,0,0,0},
/* 13 */  {R,0,0,R,0,R,R,R,R,0,R,R,R,0,0,R,R,R,R,0,R,R,R,0,0,0,B,0,B,0,0,0},
/* 14 */  {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,B,R,B,0,0,0},
/* 15 */  {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,B,0,0,0,0}
};

/**
 * Program to control a 16x32 RGB LED Matrix. To try squeeze the
 * most amount of power from the arduino platform, the control & data
 * structures are fixed in place and are as follows:
 * .------.---------.----.----.------.------.------.----.-----.-----.
 * | PORT |   USE   | 7  | 6  |  5   |  4   |  3   | 2  |  1  |  0  |
 * :------+---------+----+----+------+------+------+----+-----+-----:
 * | D    | Data    | G2 | B2 | R2   | G1   | B1   | R1 | -   | -   |
 * :------+---------+----+----+------+------+------+----+-----+-----:
 * | C    | Control | -  | -  | Line | Line | Line | OE | CLK | LAT |
 * '------'---------'----'----'------'------'------'----'-----'-----'
 *
 */

unsigned volatile char currentRow = 0;
unsigned volatile char redraw = 1;

void setup(void) {
  DDRD = 0xFF;
  DDRC = 0xFF;
}

void timer0_init(void) {
  TCCR0A = (1 << WGM01); /* Enable CTC mode */
  OCR0A = 16; /* With 1024 bit prescale, gives us period of ~= 0.0021 aka 60hz */
  TIMSK0 = (1 << OCIE0A); /* Fire interrupt on match */
  TCCR0B = (1 << CS02) | (1 << CS00); /* 1024 bit prescaler, and start timer */
}

int main(void) {
  setup();
  timer0_init();
  sei();
  for(;;){}
}

/** Update display **/
ISR(TIMER0_COMPA_vect) {
  PORTC = (currentRow << ADDR_OFFSET) | OE;
  for (int col = 0; col < M_WIDTH; col++) {
    char top = matrix[currentRow][col];
    char btm = matrix[currentRow + M_HALFHEIGHT][col];
    PORTD = top << TOP_OFFSET | btm << BTM_OFFSET;
    /* pulse clock */
    PORTC |= CLK;
    PORTC &= ~CLK;
  }
  PORTC &= ~OE;
  PORTC |= LATCH;
  currentRow = (currentRow + 1) % M_HALFHEIGHT;
}

ISR(TIMER0_COMPB_vect) {
  redraw = 1;
}


void updateFullMatrix(void) {
  for (int row = 0; row < M_HALFHEIGHT; row++) {
  }
}
