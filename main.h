#ifndef control_codes
#define control_codes

#define LATCH 0x01
#define CLK   0x02
#define OE  0x04

#define ADDR_OFFSET 3

#define R 0x01
#define G 0x02
#define B 0x04

#define TOP_OFFSET 2
#define BTM_OFFSET 5

#endif

#ifndef DIMS
#define DIMS

#define M_WIDTH   32
#define M_HEIGHT  16
#define M_HALFHEIGHT 8

#define F_RATE 10

#endif
#ifndef main_h
#define main_h
void setup(void);
int main(void);
/**
 * Writes the full bitmap out to the display
 */
void updateFullMatrix(void);
#endif

